module gitlab.com/oss-cloud/hashicorp/vault

go 1.15

require (
	github.com/hashicorp/vault/api v1.0.4
	go.opentelemetry.io/otel v0.13.0
)
