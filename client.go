package vault

import (
	"context"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"

	"github.com/hashicorp/vault/api"
	"gitlab.com/oss-cloud/hashicorp/vault/auth"
	"go.opentelemetry.io/otel/api/global"
)

var clientTracer = global.Tracer("vault-client")

// Config extend on Vault api.Config
type Config struct {
	api.Config
	AuthMethod auth.Method
}

type Client struct {
	*api.Client
}

func NewClient(ctx context.Context, c *Config) (_ *Client, err error) {
	base, err := api.NewClient(&c.Config)
	if err != nil {
		return
	}
	token, err := c.AuthMethod.InitialToken(ctx, base)
	if err != nil {
		return
	}
	base.SetToken(token)
	return &Client{
		Client: base,
	}, nil
}

func (c *Client) List(ctx context.Context, path string) (resp *api.Response, err error) {
	ctx, span := transitTracer.Start(ctx, "list")
	defer span.End()

	url := fmt.Sprintf("/v1/%s?list=true", path)
	r := c.NewRequest(http.MethodGet, url)

	resp, err = c.RawRequestWithContext(ctx, r)
	if err != nil {
		if resp != nil {
			defer resp.Body.Close()
			io.Copy(ioutil.Discard, resp.Body)
		}
		return nil, err
	}
	return resp, nil
}

func (c *Client) Read(ctx context.Context, path string) (resp *api.Response, err error) {
	ctx, span := transitTracer.Start(ctx, "read")
	defer span.End()

	url := fmt.Sprintf("/v1/%s", path)
	r := c.NewRequest(http.MethodGet, url)

	resp, err = c.RawRequestWithContext(ctx, r)
	if err != nil {
		if resp != nil {
			defer resp.Body.Close()
			io.Copy(ioutil.Discard, resp.Body)
		}
		return nil, err
	}
	return resp, nil
}

func (c *Client) Write(ctx context.Context, path string, body interface{}) (err error) {
	ctx, span := transitTracer.Start(ctx, "write")
	defer span.End()

	url := fmt.Sprintf("/v1/%s", path)
	r := c.NewRequest(http.MethodPut, url)
	if body != nil {
		if err = r.SetJSONBody(body); err != nil {
			return
		}
	}

	resp, err := c.RawRequestWithContext(ctx, r)
	if resp != nil {
		defer resp.Body.Close()
		io.Copy(ioutil.Discard, resp.Body)
	}
	if err != nil {
		return err
	}
	return nil
}

func (c *Client) WriteForResponse(ctx context.Context, path string, body interface{}) (resp *api.Response, err error) {
	ctx, span := transitTracer.Start(ctx, "write")
	defer span.End()

	url := fmt.Sprintf("/v1/%s", path)
	r := c.NewRequest(http.MethodPut, url)
	if body != nil {
		if err = r.SetJSONBody(body); err != nil {
			return
		}
	}

	resp, err = c.RawRequestWithContext(ctx, r)
	if err != nil {
		if resp != nil {
			defer resp.Body.Close()
			io.Copy(ioutil.Discard, resp.Body)
		}
		return nil, err
	}
	return resp, nil
}

func (c *Client) Delete(ctx context.Context, path string) (err error) {
	ctx, span := transitTracer.Start(ctx, "write")
	defer span.End()

	url := fmt.Sprintf("/v1/%s", path)
	r := c.NewRequest(http.MethodDelete, url)

	resp, err := c.RawRequestWithContext(ctx, r)
	if err != nil {
		if resp != nil {
			defer resp.Body.Close()
			io.Copy(ioutil.Discard, resp.Body)
		}
		return err
	}
	return nil
}
