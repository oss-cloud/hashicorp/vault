package token

import (
	"context"

	"github.com/hashicorp/vault/api"
	"gitlab.com/oss-cloud/hashicorp/vault/auth"
)

var _ auth.Method = &authMethod{}

type authMethod struct {
	Token string
}

func New(token string) auth.Method {
	return &authMethod{
		Token: token,
	}
}

func (a *authMethod) InitialToken(_ context.Context, _ *api.Client) (string, error) {
	return a.Token, nil
}
