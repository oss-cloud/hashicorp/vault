package approle

import (
	"context"

	"github.com/hashicorp/vault/api"
	"gitlab.com/oss-cloud/hashicorp/vault/auth"
	"go.opentelemetry.io/otel/api/global"
)

var tracer = global.Tracer("vault-approle-auth")

var _ auth.Method = &authMethod{}

type authMethod struct {
	auth.BaseMethod
	RoleId   string `json:"role_id"`
	SecretId string `json:"secret_id"`
}

func New(name, roleId, secretId string) auth.Method {
	return &authMethod{
		BaseMethod: auth.BaseMethod{
			Name: name,
		},
		RoleId:   roleId,
		SecretId: secretId,
	}
}

func (a *authMethod) InitialToken(ctx context.Context, client *api.Client) (token string, err error) {
	_, span := tracer.Start(ctx, "InitialToken")
	defer span.End()

	secret, err := a.BaseMethod.Write(ctx, client, a)
	if err != nil {
		return
	}
	if secret == nil {
		return "", auth.ErrNotExists
	}
	return secret.TokenID()
}
