package vault

import (
	"context"
	"encoding/json"
	"fmt"

	"go.opentelemetry.io/otel/api/global"
)

var transitTracer = global.Tracer("vault-client-transit")

type TransitKeyType string

func (c *Client) Transit(name string) Transit {
	return &transit{
		c:    c,
		name: name,
	}
}

type Transit interface{}

type transit struct {
	c    *Client
	name string
}

type TransitCreateRequest struct {
	ConvergentEncryption bool   `json:"convergent_encryption"`
	Derived              bool   `json:"derived"`
	Exportable           bool   `json:"exportable"`
	AllowPlaintextBackup bool   `json:"allow_plaintext_backup"`
	Type                 string `json:"type,omitempty"`
}

func (t *transit) Create(ctx context.Context, name string, req *TransitCreateRequest) (err error) {
	ctx, span := transitTracer.Start(ctx, "create")
	defer span.End()

	url := fmt.Sprintf("%s/keys/%s", t.name, name)
	return t.c.Write(ctx, url, req)
}

type TransitReadResponse struct {
	Data struct {
		Name                 string           `json:"name"`
		Type                 string           `json:"type"`
		Keys                 map[string]int64 `json:"keys"`
		MinDecryptionVersion int              `json:"min_decrytion_version"`
		MinEncryptionVersion int              `json:"min_encryption_version"`
		LatestVersion        int              `json:"latest_version"`
		DeletionAllowed      bool             `json:"deletion_allowed"`
		Derived              bool             `json:"derived"`
		Exportable           bool             `json:"exportable"`
		AllowPlaintextBackup bool             `json:"allow_plaintext_backup"`
		SupportsEncryption   bool             `json:"supports_encryption"`
		SupportsDecryption   bool             `json:"supports_decryption"`
		SupportsDerivation   bool             `json:"supports_derivation"`
		SupportsSigning      bool             `json:"supports_signing"`
	} `json:"data"`
}

func (t *transit) Read(ctx context.Context, name string) (_ *TransitReadResponse, err error) {
	ctx, span := transitTracer.Start(ctx, "read")
	defer span.End()

	url := fmt.Sprintf("%s/keys/%s", t.name, name)
	resp, err := t.c.Read(ctx, url)
	if err != nil {
		return
	}

	var result TransitReadResponse
	if err = json.NewDecoder(resp.Body).Decode(&result); err != nil {
		return
	}
	return &result, nil
}

type TransitListResponse struct {
	Data struct {
		Keys []string `json:"keys"`
	} `json:"data"`
}

func (t *transit) List(ctx context.Context) (_ *TransitListResponse, err error) {
	ctx, span := transitTracer.Start(ctx, "list")
	defer span.End()

	path := fmt.Sprintf("%s/keys", t.name)
	resp, err := t.c.List(ctx, path)
	if err != nil {
		return
	}

	var result TransitListResponse
	if err = json.NewDecoder(resp.Body).Decode(&result); err != nil {
		return
	}
	return &result, nil
}

func (t *transit) Delete(ctx context.Context, name string) (err error) {
	ctx, span := transitTracer.Start(ctx, "delete")
	defer span.End()

	url := fmt.Sprintf("%s/keys/%s", t.name, name)
	return t.c.Delete(ctx, url)
}

type TransitUpdateRequest struct {
	MinDecryptionVersion int   `json:"min_decrytion_version"`
	MinEncryptionVersion int   `json:"min_encryption_version"`
	DeletionAllowed      *bool `json:"deletion_allowed,omitempty"`
	Exportable           *bool `json:"exportable,omitempty"`
	AllowPlaintextBackup *bool `json:"allow_plaintext_backup,omitempty"`
}

func (t *transit) Update(ctx context.Context, name string, req *TransitUpdateRequest) (err error) {
	ctx, span := transitTracer.Start(ctx, "update")
	defer span.End()

	url := fmt.Sprintf("%s/keys/%s/config", t.name, name)
	return t.c.Write(ctx, url, req)
}

func (t *transit) Rotate(ctx context.Context, name string) (err error) {
	ctx, span := transitTracer.Start(ctx, "update")
	defer span.End()

	url := fmt.Sprintf("%s/keys/%s/rotate", t.name, name)
	return t.c.Write(ctx, url, nil)
}

type TransitKeyExportResponse struct {
	Data struct {
		Keys map[string]string
	} `json:"data"`
}

func (t *transit) Export(ctx context.Context, keyType, name string) (_ *TransitKeyExportResponse, err error) {
	ctx, span := transitTracer.Start(ctx, "export")
	defer span.End()

	url := fmt.Sprintf("%s/export/%s/%s", t.name, keyType, name)
	resp, err := t.c.Read(ctx, url)
	if err != nil {
		return
	}
	var result TransitKeyExportResponse
	if err = json.NewDecoder(resp.Body).Decode(&result); err != nil {
		return
	}
	return &result, nil
}

func (t *transit) ExportVersion(ctx context.Context, keyType, name, version string) (_ *TransitKeyExportResponse, err error) {
	ctx, span := transitTracer.Start(ctx, "exportVersion")
	defer span.End()

	url := fmt.Sprintf("%s/export/%s/%s/%s", t.name, keyType, name, version)
	resp, err := t.c.Read(ctx, url)
	if err != nil {
		return
	}
	var result TransitKeyExportResponse
	if err = json.NewDecoder(resp.Body).Decode(&result); err != nil {
		return
	}
	return &result, nil
}

type TransitEncryptRequest struct {
	Plaintext  string `json:"plaintext"`
	Context    string `json:"context,omitempty"`
	KeyVersion int    `json:"key_version,omitempty"`
}

type TransitEncryptResponse struct {
	Data struct {
		Ciphertext string `json:"ciphertext"`
	} `json:"data"`
}

func (t *transit) Encrypt(ctx context.Context, name string, req *TransitEncryptRequest) (_ *TransitEncryptResponse, err error) {
	ctx, span := transitTracer.Start(ctx, "encrypt")
	defer span.End()

	url := fmt.Sprintf("%s/encrypt/%s", t.name, name)
	resp, err := t.c.WriteForResponse(ctx, url, req)
	if err != nil {
		return
	}
	var result TransitEncryptResponse
	if err = json.NewDecoder(resp.Body).Decode(&result); err != nil {
		return
	}
	return &result, nil
}

type TransitDecryptRequest struct {
	Ciphertext string `json:"ciphertext"`
	Context    string `json:"context,omitempty"`
}

type TransitDecryptResponse struct {
	Data struct {
		Plaintext string `json:"plaintext"`
	} `json:"data"`
}

func (t *transit) Decrypt(ctx context.Context, name string, req *TransitDecryptRequest) (_ *TransitDecryptResponse, err error) {
	ctx, span := transitTracer.Start(ctx, "decrypt")
	defer span.End()

	url := fmt.Sprintf("%s/decrypt/%s", t.name, name)
	resp, err := t.c.WriteForResponse(ctx, url, req)
	if err != nil {
		return
	}
	var result TransitDecryptResponse
	if err = json.NewDecoder(resp.Body).Decode(&result); err != nil {
		return
	}
	return &result, nil
}

type TransitRewrapRequest struct {
	Ciphertext string `json:"ciphertext"`
	Context    string `json:"context,omitempty"`
	KeyVersion int    `json:"key_version,omitempty"`
}

type TransitRewrapResponse struct {
	Data struct {
		Ciphertext string `json:"ciphertext"`
	} `json:"data"`
}

func (t *transit) Rewrap(ctx context.Context, name string, req *TransitRewrapRequest) (_ *TransitRewrapResponse, err error) {
	ctx, span := transitTracer.Start(ctx, "encrypt")
	defer span.End()

	url := fmt.Sprintf("%s/rewrap/%s", t.name, name)
	resp, err := t.c.WriteForResponse(ctx, url, req)
	if err != nil {
		return
	}
	var result TransitRewrapResponse
	if err = json.NewDecoder(resp.Body).Decode(&result); err != nil {
		return
	}
	return &result, nil
}

type TransitDataKeyRequest struct {
	Context string `json:"context,omitempty"`
	Bits    int    `json:"bits,omitempty"`
}

type TransitDataKeyResponse struct {
	Data struct {
		Ciphertext string `json:"ciphertext"`
		Plaintext  string `json:"plaintext,omitempty"`
	} `json:"data"`
}

func (t *transit) DataKey(ctx context.Context, keyType, name string, req *TransitDataKeyRequest) (_ *TransitDataKeyResponse, err error) {
	ctx, span := transitTracer.Start(ctx, "encrypt")
	defer span.End()

	url := fmt.Sprintf("%s/datakey/%s/%s", t.name, keyType, name)
	resp, err := t.c.WriteForResponse(ctx, url, req)
	if err != nil {
		return
	}
	var result TransitDataKeyResponse
	if err = json.NewDecoder(resp.Body).Decode(&result); err != nil {
		return
	}
	return &result, nil
}
